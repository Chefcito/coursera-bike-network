var bike = require('../models/bike');

// Bike main page.
exports.bikeList = function (req, res) {
    res.render('bikes/index', {bikeList: bike.bikeList});
}

// Add a new bike.
exports.createBikeGet = function (req, res) {
  res.render('bikes/add-bike');
}

exports.createBikePost = function (req, res) {
  let newBike = new bike(req.body.id, req.body.color, req.body.model, [req.body.lat, req.body.lng]);
  bike.addBike(newBike);
  res.redirect('/bike');
}

// Update a bike by its ID.
exports.updateBikeGet = function (req, res) {
  let mBike = bike.findBike(req.params.id);
  res.render('bikes/update-bike', {bike: mBike});
}

exports.updateBikePost = function (req, res) {
  let mBike = bike.findBike(req.body.id);
  mBike.color = req.body.color;
  mBike.model = req.body.model;
  mBike.location = [req.body.lat, req.body.lng];
  res.redirect('/bike');
}

// Delete a bike.
exports.deleteBike = function (req, res) {
  bike.removeBike(req.body.id);
  res.redirect('/bike');
}