var bike = require('../../models/bike');

exports.bikeList = function (req, res) {
  res.status(200).json({
    bikes: bike.bikeList,
  });
} 

exports.createBike = function (req, res) {
  let mBike = new bike(req.body.id, req.body.color, req.body.model, [req.body.lat, req.body.lng]);
  bike.addBike(mBike);

  res.status(200).json({
    bike: mBike
  });
} 

exports.updateBike = function (req, res) {
  let mBike = bike.findBike(req.body.id);
  mBike.color = req.body.color;
  mBike.model = req.body.model;
  mBike.location = [req.body.lat, req.body.lng];

  res.status(200).json({
    bike: mBike
  });
}

exports.deleteBike = function (req, res) {
  let mBike = bike.findBike(req.body.id);
  bike.removeBike(req.body.id)
  
  res.status(200).json({
    bike: mBike
  });
}