var bike = function (id, color, model, location) {
    this.id = id;
    this.color = color;
    this.model = model;
    this.location = location;
}

bike.prototype.toString = function () {
    return 'id: ' + this.id + '\n color: ' + this.color; 
}

bike.bikeList = [];

bike.addBike = function (newBike) {
    bike.bikeList.push(newBike);
}

bike.findBike = function (bikeId) {
  let mBike = bike.bikeList.find(bike => bike.id == bikeId);
  if (mBike) {
    return mBike;
  }
  else  {
    throw new Error(`No existe una bicicleta con el id ${bikeId}`);
  }
}

bike.updateBike = function (bikeId, newBike) {
  let mBike = bike.bikeList.find(bike => bike.id == bikeId);
  let index = bike.bikeList.indexOf(mBike);
  bike.bikeList.splice(index, 1, newBike)
}

bike.removeBike = function (bikeId) {
  bike.findBike(bikeId);
  for(let i = 0; i < bike.bikeList.length; i++) {
    if(bike.bikeList[i].id == bikeId) {
      bike.bikeList.splice(i, 1);
      break;
    }
  }  
}

// Add some bikes
var bike1 = new bike(1, 'Rojo', 'Urbano', [3.4038203, -76.5364438]);
var bike2 = new bike(2, 'Azul Verdoso', 'Urbano', [3.4035764, -76.5339519,]);

bike.addBike(bike1);
bike.addBike(bike2);

module.exports = bike;