var bike = require('../../models/bike');
var request = require('request');
var server = require('../../bin/www');

describe('Bike List API', () => {
  beforeEach(() => {
    bike.bikeList = [];
  });
  
  it('GET /api/bike', () => {
    let mBike = new bike(1, 'Azul Verdoso', 'Urbano', [3.4035764, -76.5339519,]);
    bike.addBike(mBike);

    request.get('http://localhost:3000/api/bike', (error, response, body) => {
      expect(response.statusCode).toBe(200);
    });
  });

  it('POST /api/bike/create-bike', (done) => {
    let headers = {'content-type': 'application/json'};
    let mBike = '{"id": 1, "color": "Negro", "model": "Urbano", "lat": 3.4035764, "lng": -76.5339519}';

    request.post({
      headers: headers,
      url: 'http://localhost:3000/api/bike/create-bike',
      body: mBike
    }, (error, response, body) => {
      expect(response.statusCode).toBe(200);
      expect(bike.findBike(1).color).toBe('Negro');
      done();
    });
  });

  it('PUT /api/bike/update-bike', (done) => {
    let mBike = new bike(1, 'Azul Verdoso', 'Urbano', [3.4035764, -76.5339519,]);
    bike.addBike(mBike);

    let headers = {'content-type': 'application/json'};
    let newBike = '{"id": 1, "color": "Blanco", "model": "Todoterreno", "lat": 3.4015764, "lng": -76.5312519}';

    request.put({
      headers: headers,
      url: 'http://localhost:3000/api/bike/update-bike',
      body: newBike
    }, (error, response, body) => {
      expect(response.statusCode).toBe(200);
      expect(bike.findBike(1).model).toBe('Todoterreno');
      done();
    });
  });

  it('DELETE /api/bike/delete-bike', (done) => {
    let mBike = new bike(1, 'Azul Verdoso', 'Urbano', [3.4035764, -76.5339519,]);
    bike.addBike(mBike); 
    expect(bike.bikeList.length).toBe(1);
    
    let headers = {'content-type': 'application/json'};
    let bikeId = '{"id": 1}';

    request.delete({
      headers: headers,
      url: 'http://localhost:3000/api/bike/delete-bike',
      body: bikeId
    }, (error, response, body) => {
      expect(response.statusCode).toBe(200);
      expect(bike.bikeList.length).toBe(0);
      done();
    });
  });
});