var bike = require('../../models/bike');

describe('Bike list', () => {
  beforeEach(() => {
    bike.bikeList = [];
  });

  it('is empty', () => {
    expect(bike.bikeList.length).toBe(0);
  });

  it('adds a new bike', () => {
    let mBike = new bike(1, 'Azul Verdoso', 'Urbano', [3.4035764, -76.5339519,]);
    bike.addBike(mBike);
    
    expect(bike.bikeList.length).toBe(1);
    expect(bike.bikeList[0]).toBe(mBike);
  });

  it('finds a bike', () => {
    let mBike = new bike(1, 'Blanco', 'Montaña', [3.4038203, -76.5364438]);
    bike.addBike(mBike);
    
    expect(bike.findBike(1)).toBe(mBike);
  });

  it('updates a bike', () => {
    let mBike = new bike(1, 'Blanco', 'Montaña', [3.4038203, -76.5364438]);
    bike.addBike(mBike);
    let newBike = bike.findBike(1);
    newBike.color = 'Negro';
    expect(bike.findBike(1).color).toBe('Negro');
  });

  it('removes a bike', () => {
    let mBike = new bike(1, 'Blanco', 'Montaña', [3.4038203, -76.5364438]);
    bike.addBike(mBike);   
    expect(bike.bikeList.length).toBe(1);
    bike.removeBike(mBike.id);
    expect(bike.bikeList.length).toBe(0);
  })
});