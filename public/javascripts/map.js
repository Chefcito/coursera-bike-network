var myMap = L.map('main_map').setView([3.4041967,-76.5338897], 20);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(myMap);

$.ajax({
  dataType: "json",
  url: "api/bike",
  success: function(result) {
    console.log(result);
    result.bikes.forEach((bike) => {
      L.marker(bike.location, {
        title: bike.id
      }).addTo(myMap)
    }); 
  }
});