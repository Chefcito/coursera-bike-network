var express = require('express');
var router = express.Router();

var bikeController = require('../controllers/bike');

router.get('/', bikeController.bikeList);

router.get('/add-bike', bikeController.createBikeGet);
router.post('/add-bike', bikeController.createBikePost);

router.get('/:id/update-bike/', bikeController.updateBikeGet);
router.post('/:id/update-bike', bikeController.updateBikePost);

router.post('/:id/delete-bike', bikeController.deleteBike);

module.exports = router;  
