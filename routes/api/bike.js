var express = require('express');
var router = express.Router();

var bikeController = require('../../controllers/api/bike');

router.get('/', bikeController.bikeList);
router.post('/create-bike', bikeController.createBike);
router.put('/update-bike', bikeController.updateBike);
router.delete('/delete-bike', bikeController.deleteBike);

module.exports = router;
